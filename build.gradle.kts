val logbackVersion = "1.2.3"
val ktorVersion = "2.0.0"
val kotlinVersion = "1.6.21"

plugins {
    val kotlinVersion = "1.6.21"
    application
    kotlin("jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion
}

val versionMajor = 1    // Пилотный выпуск.
val versionMinor = 0    //
val versionPatch = 0    //

group = "com.chit"
version = "$versionMajor.$versionMinor.$versionPatch"

application {
    mainClass.set("com.chit.ApplicationKt")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
}

dependencies {

    // Ktor + сериализация
    implementation("io.ktor:ktor-server-content-negotiation-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-core-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-netty-jvm:$ktorVersion")
    implementation("io.ktor:ktor-serialization-kotlinx-json-jvm:$ktorVersion")

    // SQL
    implementation("mysql:mysql-connector-java:8.0.28")

    // Логгирование
    implementation("ch.qos.logback:logback-classic:$logbackVersion")

    // Тестирование
    testImplementation("io.ktor:ktor-server-tests-jvm:$ktorVersion")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlinVersion")
}