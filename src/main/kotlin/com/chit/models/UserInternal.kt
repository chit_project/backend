package com.chit.models

import kotlinx.serialization.Serializable

@Serializable
data class UserInternal(
    val login: String,
    val password: String,
    val nickName: String,
    val role: String
)