package com.chit.models

fun UserInternal.toUserRepo(): UserRepo = UserRepo(
    login = login,
    nickName = nickName,
    role = role
)