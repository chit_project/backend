package com.chit.models

import kotlinx.serialization.Serializable

@Serializable
data class UserRepo(
    val login: String,
    val nickName: String,
    val role: String
)

fun getDefaultUserRepo(): UserRepo =
    UserRepo(
        login = "",
        nickName = "",
        role = ""
    )