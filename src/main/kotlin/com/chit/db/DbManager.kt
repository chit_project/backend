package com.chit.db

import com.chit.models.UserInternal
import com.chit.models.UserRepo
import com.chit.models.getDefaultUserRepo
import com.chit.models.toUserRepo
import java.sql.*

object DbManager {

    private const val JDBC_DRIVER = "com.mysql.cj.jdbc.Driver"
    private const val DB_URL =
        "jdbc:mysql://localhost/chit?enabledTLSProtocols=TLSv1.2&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Moscow&useSSL=false"

    private const val DB_USER = "chit_user2"
    private const val DB_PASSWORD = "chit_user2"

    init {
        try {
            Class.forName(JDBC_DRIVER)
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
    }

    fun getUserBy(login: String, password: String, connect: Connection? = null): UserRepo {
        val connection = connect ?: getConnection()
        val user = getUserFromDb(login, password, connection) ?: getDefaultUserRepo()
        connection.close()
        return user
    }

    /**
     * Функция записывает пользователя в базу и затем возвращает данного записанного пользователя.
     * Если в возвращаемом объекте [UserRepo.login] пустой, значит такой пользователь уже есть в базе.
     */
    fun insert(user: UserInternal): UserRepo {
        val userRepo: UserRepo = getUserBy("\"${user.login}\"", user.password)
        return if (userRepo.login.isBlank()) {
            val connection = getConnection()
            try {
                connection.autoCommit = false
                // 1 or more queries or updates

                insertUser(user, connection)

                connection.commit()
            } catch (e: Exception) {
                connection.rollback()
            } finally {
                connection.close()
            }
            val ur = getUserBy("\"${user.login}\"", user.password)
            ur
        } else {
            userRepo.copy(login = "")
        }
    }

    private fun getUserFromDb(login: String, password: String, connection: Connection): UserRepo? {
        val sqlQuery = "SELECT * FROM `chit_users` WHERE login=$login"
        val statement = connection.createStatement()
        val resultSet = statement.executeQuery(sqlQuery)
        val user = getUserFrom(resultSet)
        resultSet.close()
        statement.close()
        val isCorrectPassword = user?.password == password
        return if (isCorrectPassword) user?.toUserRepo() else null
    }

    private fun insertUser(user: UserInternal, connection: Connection) {

        val sqlQuery = "INSERT INTO chit_users (" +
                "login," +
                "password," +
                "nickName," +
                "role" +
                ") VALUES (?,?,?,?);"

        with(connection) {

            prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS)
                .run {
                    getStatement(this, user)
                }
                .run {
                    executeUpdate()
                    this
                }
                .close()
        }
    }

    private fun getStatement(statement: PreparedStatement, user: UserInternal): PreparedStatement {
        return statement.apply {
            setString(1, user.login)
            setString(2, user.password)
            setString(3, user.nickName)
            setString(4, user.role)
        }
    }

    private fun getUserFrom(resultSet: ResultSet): UserInternal? {
        var user: UserInternal? = null
        while (resultSet.next()) {
            user = UserInternal(
                login = resultSet.getString("login"),
                password = resultSet.getString("password"),
                nickName = resultSet.getString("nickName"),
                role = resultSet.getString("role")
            )
        }
        return user
    }

    private fun getConnection() = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)
}