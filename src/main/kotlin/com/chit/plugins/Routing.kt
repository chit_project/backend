package com.chit.plugins

import com.chit.db.DbManager
import com.chit.models.UserInternal
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Application.configureRouting() {

    // Starting point for a Ktor app:
    routing {

        post("/post_user") {
            val user = call.receive<UserInternal>()
            if (user.login.isBlank()) {
                call.respondText("The User's e-mail cannot be blank!", status = HttpStatusCode.BadRequest)
            } else {
                val userRepo = DbManager.insert(user)
                if (userRepo.login.isBlank()) {
                    call.respondText("Such user already exists", status = HttpStatusCode.UnprocessableEntity)
                } else {
                    call.respond(userRepo)
                }
            }
        }

        get("/get_user") {
            val login = call.request.queryParameters["login"]
            val password = call.request.queryParameters["password"]
            if (login.isNullOrBlank() || password.isNullOrBlank()) {
                call.respondText(
                    "The User's e-mail or password cannot be null or blank!",
                    status = HttpStatusCode.BadRequest
                )
            } else {
                val user = DbManager.getUserBy("\"$login\"", password)
                call.respond(user)
            }
        }
    }
}
