package com.chit

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.chit.plugins.*

fun main(args: Array<String>) {

    embeddedServer(
        Netty,
        host = "82.146.40.21",
        port = 8070
    ) {
        configureSerialization()
        configureRouting()
    }.start(wait = true)
}